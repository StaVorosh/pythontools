import os
import sys

import signal
import tempfile

from ..cmd import Cmd
from ..myconfig import MyConfig
from ..myfileutils import *


class Server(MyConfig):
    __verbose = False
    __terminating = False
    __cmds = None
    _is_run = False

    @property
    def cmd(self):
        """
        Creates new Cmd instance
        :rtype: Cmd
        """
        cmd = Cmd()
        cmd.verbose = self.__verbose
        self.__cmds.append(cmd)
        return cmd

    @property
    def _my_env(self):
        """
        Override this in child classes to have static env. variables while running commands

        :return: Dictionary of environment variables (name:value)
        :rtype: dict
        """
        return {}

    def __init__(self, verbose, conf_folder, sections):
        self.__cmds = []
        self.__conf_folder = conf_folder
        if not os.path.isdir(conf_folder):
            os.makedirs(conf_folder)

        super(Server, self).__init__(conf_folder, sections)

        self.__verbose = verbose

        for sig in [signal.SIGTERM, signal.SIGINT, signal.SIGHUP, signal.SIGQUIT]:
            signal.signal(sig, self.__signal_term_handler)

    def _get_env(self, args=None):
        """
        Get environment variables to execute command
        Contains from static class variables (self._my_env) extended with args and with python process external variables

        :param args:
        :return:
        """
        env = self._my_env
        if args is not None:
            env.update(args)
        env.update(os.environ)
        return env

    @property
    def _my_files(self):
        """
        Override this in child classes to save and restore files to and from persistent folder

        * 'default_path' should contain root path of default application location
        * 'persist_path' should contain root path of persistent folder
        * 'files' should contain list of files to save and restore, including relative (to default_path) paths
        :rtype: dict

        May contain several sections like this::

        > {
        >   'section1': {
        >     'default_path': self._get_env()['HOME'],
        >     'persist_path': self.conf_folder,
        >     'files': []
        >   },
        >   'section2': {
        >     'default_path': self._get_env()['HOME'],
        >     'persist_path': self.conf_folder,
        >     'files': []
        >   }
        > }
        """
        return {
            'default_path': self._get_env()['HOME'],
            'persist_path': self.conf_folder,
            'files': []
        }

    def _save_files(self, section=None):
        """
        Move list of files from their default location into persistent folder.
        Paths and files are defined in self._my_files property

        :param section: choose files section if there are several of them
        :type section: str
        """
        files = self._my_files
        if not section is None:
            files = files[section]
        movefiles(files['default_path'], files['persist_path'], *files['files'])

    def _restore_files(self, section=None):
        """
        Make links to files from persistent folder to their default location.
        Paths and files are defined in self._my_files property

        :param section: choose files section if there are several of them
        :type section: str
        """
        files = self._my_files
        if not section is None:
            files = files[section]
        makelinks(files['persist_path'], files['default_path'], *files['files'])

    def run(self, command, verbose_force=None, env=None, timeout=None):
        """
        Executes shell command with new Cmd() object

        :param command: Shell command line (or lines)
        :type command: str
        :param verbose_force: Set to True to see output in stdout. Defaults to False
        :type verbose_force: bool
        :param env: Environment variables would be passed to shell. Defaults to self._get_env()
        :type env: dict
        :param timeout: Timeout in seconds after which execution is stopped. Defaults to None
        :type timeout: float
        :raises MyTimeoutError: If timeout reached while executing
        """
        if env is None:
            env = self._get_env()
        self.cmd.run(command, verbose_force, env, timeout)

    def __signal_term_handler(self, signal, frame):
        """
        Handler which is called when python process catches system termination signal
        """
        if not self.__terminating:
            self.__terminating = True
            for cmd in self.__cmds:
                cmd.kill()
            self.stop()
            sys.exit(0)

    def stop(self):
        """
        Override this in child classes to stop some processes while handling system termination signal
        :return:
        """
        pass
