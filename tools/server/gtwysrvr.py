import os
import tempfile

from ..myfileutils import *
from server import Server


class Gtwysrvr(Server):
    """
    Siebel gateway server management class

    :param verbose: Set to True to see executed commands and output in stdout. Defaults to False
    :type verbose: bool
    :param conf_folder: Path to folder where configuration and data files are stored
    :type conf_folder: str

    After configuration moves needed files to configuration folder.
    Before starting gateway it makes links to this files from default locations
    """

    def __init__(self, verbose, conf_folder):
        super(Gtwysrvr, self).__init__(verbose, conf_folder, {
            'gateway': ['host', 'port', 'siebel_fs'],
            'enterprise': ['name'],
            'db': ['host', 'port', 'sid', 'tns']
        })

    def __gtwy_configured(self):
        return 'gateway' in self.sections()

    def __ent_configured(self):
        return 'enterprise' in self.sections()

    @property
    def _my_env(self):
        return {
            'GATEWAY_HOST': self.gateway.host,
            'GATEWAY_PORT': self.gateway.port,
            'SIEBEL_FS': self.gateway.siebel_fs,
            'SIEBEL_ENTERPRISE': self.enterprise.name,
            'ORACLE_DB_TNS': self.db.tns,
            'ORACLE_DB_HOST': self.db.host,
            'ORACLE_DB_PORT': self.db.port,
            'ORACLE_DB_SID': self.db.sid
        }

    @property
    def _my_files(self):
        return {
            'gateway': {
                'default_path': self._get_env()['ORACLE_HOME'] + '/gtwysrvr',
                'persist_path': self.conf_folder,
                'files': [
                    '/admin/siebns.dat',
                    '/bin/start_ns',
                    '/siebenv.sh',
                    '/sys'
                ]
            },
            'enterprise': {
                'default_path': self._get_env()['ORACLE_HOME'] + '/gtwysrvr',
                'persist_path': self.conf_folder,
                'files': [
                    '/admin/srvrdefs.run',
                    '/admin/srvrdefs_sia.run',
                    '/bin/gateway.cfg'
                ]
            }
        }

    def start(self):
        self.run("""
            . $ORACLE_HOME/gtwysrvr/siebenv.sh > /dev/null
            start_ns
        """, verbose_force=True)
        self._is_run = True

    def stop(self, force=False):
        if self._is_run or force:
            self._is_run = False
            self.run("""
                . $ORACLE_HOME/gtwysrvr/siebenv.sh > /dev/null
                stop_ns
            """, verbose_force=True)

    def initgtwy(self, host, port, siebel_fs):
        assert not self.__gtwy_configured(), 'Gateway is already configured'
        self.gateway.host = host
        self.gateway.port = port
        self.gateway.siebel_fs = siebel_fs
        os.makedirs(siebel_fs)

        print ('\n*** Start gateway %s:%s configuration' % (self.gateway.host, self.gateway.port))

        print ('* Prepare gateway configuration response file')
        tempdir = tempfile.mkdtemp()
        self.run("envsubst ${HOME}/templates/gateway_config.rsp > ${TEMP_DIR}/gateway_config.rsp", env=self._get_env({
            'TEMP_DIR': tempdir
        }))

        print ('* Execute configuration')
        self.run("""
            . $ORACLE_HOME/gtwysrvr/cfgenv.sh
            cd $ORACLE_HOME/config
            ./config.sh -mode enterprise -responsefile ${TEMP_DIR}/gateway_config.rsp -verbose | tee ${TEMP_DIR}/config.log
            #cat $ORACLE_HOME/cfgtoollogs/cfg/*
            grep -iq error ${TEMP_DIR}/config.log && exit 1
            exit 0
        """, env=self._get_env({
            'TEMP_DIR': tempdir
        }))

        print ("* Stop gateway")
        self.stop(force=True)

        print ('* Move some files to configuration folder')
        self._save_files('gateway')

    def initent(self, name, db_host, db_port, db_sid, db_tns, sadmin_password):
        assert self.__gtwy_configured(), 'Gateway isn\'t configured yet. First configure gateway'
        assert not self.__ent_configured(), 'Enterprise is already configured'
        self.enterprise.name = name
        self.db.host = db_host
        self.db.port = db_port
        self.db.sid = db_sid
        self.db.tns = db_tns

        print ('\n*** Start enterprise %s in gateway %s:%s configuration' % (
            self.enterprise.name, self.gateway.host, self.gateway.port))

        print ('* Link files from configuration folder')
        self._restore_files('gateway')

        print ("* Start gateway")
        self.start()

        print ('* Prepare tns names file')
        self.run("envsubst ${HOME}/templates/tnsnames.ora > ${HOME}/tnsnames.ora")

        print ('* Prepare enterprise configuration response file')
        tempdir = tempfile.mkdtemp()
        self.run("envsubst ${HOME}/templates/enterprise_config.rsp > ${TEMP_DIR}/enterprise_config.rsp",
                 env=self._get_env({
                     'TEMP_DIR': tempdir,
                     'SADMIN_PASSWORD': sadmin_password
                 }))

        print ('* Execute configuration')
        self.run("""
            . $ORACLE_HOME/gtwysrvr/cfgenv.sh
            cd $ORACLE_HOME/config
            ./config.sh -mode enterprise -responsefile ${TEMP_DIR}/enterprise_config.rsp -verbose | tee ${TEMP_DIR}/config.log
            #cat $ORACLE_HOME/cfgtoollogs/cfg/*
            grep -iq error ${TEMP_DIR}/config.log && exit 1
            exit 0
        """, env=self._get_env({
            'TEMP_DIR': tempdir
        }))

        print ("* Stop gateway")
        self.stop()

        print ('* Move some files to configuration folder')
        self._save_files('enterprise')

    def rungtwy(self):
        assert self.__gtwy_configured(), 'Gateway isn\'t configured yet. First configure gateway'
        assert self.__ent_configured(), 'Enterprise isn\'t configured yet. First configure enterprise'

        print (
            '\n*** Run gateway %s:%s with enterprise %s' % (self.gateway.host, self.gateway.port, self.enterprise.name))

        print ('* Prepare tns names file')
        self.run("envsubst ~/templates/tnsnames.ora > ~/tnsnames.ora")

        print ('* Link files from configuration folder')
        self._restore_files('gateway')
        self._restore_files('enterprise')

        self.run("touch $ORACLE_HOME/gtwysrvr/log/NameSrvr.log")

        print ("* Start gateway")
        self.start()

        print ('* Output log file')
        self.run("tail -n 1000 -F $ORACLE_HOME/gtwysrvr/log/NameSrvr.log", verbose_force=True)

    def initswse(self, login, password, token):
        swse_folder = self.conf_folder + '/admin/Webserver'

        assert self.__gtwy_configured(), 'Gateway isn\'t configured yet. First configure gateway'
        assert self.__ent_configured(), 'Enterprise isn\'t configured yet. First configure enterprise'
        assert not os.path.isfile(swse_folder + '/eapps.cfg'), 'SWSE Profile already configured'

        print ('\n*** Configure swse profile for gateway %s:%s with enterprise %s' % (
            self.gateway.host, self.gateway.port, self.enterprise.name))

        print ('* Prepare swse configuration response file')
        self.cmd.run("""
      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME/gtwysrvr/lib
      export SWSE_LOGIN_PASSWORD_ENCRYPTED=$($ORACLE_HOME/gtwysrvr/bin/encryptstring $SWSE_LOGIN_PASSWORD)
      export SIEBEL_ENTERPRISE_SECURITY_TOKEN_ENCRYPTED=$($ORACLE_HOME/gtwysrvr/bin/encryptstring $SIEBEL_ENTERPRISE_SECURITY_TOKEN)
      envsubst ~/templates/swse_profile_config.rsp > /tmp/swse_profile_config.rsp
    """, env=self.get_env({
            'SWSE_PROFILE_CONF': swse_folder,
            'SWSE_LOGIN_USER_NAME': login,
            'SWSE_LOGIN_PASSWORD': password,
            'SIEBEL_ENTERPRISE_SECURITY_TOKEN': token
        }))

        print ('* Execute configuration')
        self.cmd.run("""
      . $ORACLE_HOME/gtwysrvr/cfgenv.sh
      cd $ORACLE_HOME/config
      ./config.sh -mode enterprise -responsefile /tmp/swse_profile_config.rsp -verbose | tee /tmp/config.log
      # cat $ORACLE_HOME/cfgtoollogs/cfg/*
      grep -iq error /tmp/config.log && exit 1
      exit 0
    """)
