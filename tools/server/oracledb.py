import glob
import os
import re
import tempfile

from ..myfileutils import *
from server import Server


class Oracledb(Server):
    def __init__(self, verbose, conf_folder):
        """
        Oracle database management class

        :param verbose: Set to True to see executed commands and output in stdout. Defaults to False
        :type verbose: bool
        :param conf_folder: Path to folder where configuration and data files are stored
        :type conf_folder: str

        After creating new database class moves neede files to configuration folder.
        Before starting database it makes links to this files from default locations

        Configuration folder structure::

        > ${DB_CONF}/
        >     config.ini                   - custom configuration file
        >     dbs/
        >         init${ORACLE_SID}.ora    - database parameters file
        >         hc_${ORACLE_SID}.dat     - healthcheck file
        >         lk${ORACLE_SID}          - instance lock file
        >         orapw${ORACLE_SID}       - password file
        >         ${ORACLE_SID}_bitmap.dbf - ???
        >     oradata/
        >         ${ORACLE_SID}/           - data files
        >     fast_recovery_area/          - fast recovery area
        >     admin/
        >         ${ORACLE_SID}/
        >             adump/               - audit files
        >             dpdump/              - data pupm files
        """
        super(Oracledb, self).__init__(verbose, conf_folder, {
            'db': ['sid']
        })

    def __db_configured(self):
        """
        Checks if configuration folder already contains data files

        :return: True if data files already exist, otherwise False
        :rtype: bool
        """
        return os.path.isdir(self.conf_folder + '/oradata')

    @property
    def _my_env(self):
        return {
            'DB_CONF': self.conf_folder,
            'ORACLE_SID': self.db.sid
        }

    @property
    def _my_files(self):
        return {
            'default_path': self._get_env()['ORACLE_HOME'],
            'persist_path': self.conf_folder,
            'files': [
                '/dbs/init' + self.db.sid + '.ora',
                '/dbs/hc_' + self.db.sid + '.dat',
                '/dbs/lk' + self.db.sid,
                '/dbs/orapw' + self.db.sid,
                '/dbs/' + self.db.sid + '_bitmap.dbf'
            ]
        }

    def _sqlplus(self, command, env=None, timeout=None):
        """
        Execute sql statement on oracle db
        :param command: sql statement to execute
        :type command: str
        :param env: environment variables
        :type env: dict
        :param timeout: timeout of execution
        :type timeout: float
        """
        sql_file = tempfile.NamedTemporaryFile(mode='w', suffix='.sql', delete=False)
        sql_file.write('\n'.join([
            'WHENEVER SQLERROR EXIT SQL.SQLCODE',
            'WHENEVER OSERROR EXIT FAILURE',
            command,
            'exit',
            ''
        ]))
        sql_file.close()
        self.run('\n'.join([
            'sqlplus / as sysdba @%s' % sql_file.name,
            'echo $?'
        ]), env=self._get_env(env), timeout=timeout)
        os.remove(sql_file.name)

    def start(self):
        """
        Start oracle database and listener
        """
        self._sqlplus("startup")
        self.run("lsnrctl start")
        self._is_run = True

    def stop(self, force=False):
        """
        Stop listener and oracle database
        Stop procedures are executed only if self.__is_run is True

        :param force: set to True to execute stop procedures regardless self.__is_run. Defaults to False
        :type force: bool
        """
        if self._is_run or force:
            self._is_run = False
            self.run("lsnrctl stop || true")
            self._sqlplus("shutdown immediate", env=self._get_env())

    def createdb(self, sid, sys_password):
        """
        Create new empty database and move key files to configuration folder

        :param sid: Database SID
        :type sid: str
        :param sys_password: Password for SYS and SYSTEM users
        :type sys_password: str
        """
        assert not self.__db_configured(), 'Database already exists'
        self.db.sid = sid

        print ('\n*** Start database %s creation' % self.db.sid)

        print ('* Prepare database template file')
        db_template_file = self._get_env()['HOME'] + '/dbtemplate.dbc'
        shutil.copy(self._get_env()['ORACLE_HOME'] + '/assistants/dbca/templates/General_Purpose.dbc',
                    db_template_file)
        replaceAll(db_template_file, '{ORACLE_BASE}', self.conf_folder)
        replaceAll(db_template_file, '{ORACLE_HOME}/dbs', self.conf_folder + '/dbs')

        print ('* Execute creation')
        self.run("""
            ! ${ORACLE_HOME}/bin/dbca -silent \
                -gdbname ${ORACLE_SID} \
                -sid ${ORACLE_SID} \
                -sysPassword ${SYS_PASSWORD} \
                -systemPassword ${SYS_PASSWORD} \
                -characterSet AL32UTF8 \
                -responseFile ~/templates/dbca.rsp \
                -templateName ${ORACLE_DB_TEPMLATE}

            if [ ! $? -eq 1 ]; then
                cat $ORACLE_BASE/cfgtoollogs/dbca/$ORACLE_SID.log
                exit 1
            fi
        """, env=self._get_env({
            'SYS_PASSWORD': sys_password,
            'ORACLE_DB_TEPMLATE': db_template_file
        }))

        print ("* Stop database")
        self.stop(force=True)

        print ('* Move some files to configuration folder')
        self.run('touch ${ORACLE_HOME}/dbs/${ORACLE_SID}_bitmap.dbf')
        self._save_files()

    def backupdb(self, backup_path):
        """
        Backup database using rman
        This backup could be used to start numerous databases with clonedb

        :param backup_path: Path to folder where to store backup files
        :type backup_path: str

        Backup folder structure will be::

        >   ${BACKUP_PATH}/
        >       init${ORACLE_SID}.ora - database parameters file
        >       data/                 - backup files
        """
        assert self.__db_configured(), 'Database not found'

        print ('\n*** Run database %s backup' % self.db.sid)

        print ('* Link files from configuration folder')
        self._restore_files()

        print ('* Make backup folder')
        if not os.path.isdir(backup_path + '/data'):
            os.makedirs(backup_path + '/data')

        print ("* Start database")
        self.start()

        print ("* Start backup")
        self.run("""
            ${ORACLE_HOME}/bin/rman target=/ <<EOF
            run {
                shutdown immediate;
                startup mount;
                set nocfau;
                backup as copy database format '${BACKUP_PATH}/data/%U';
                alter database open;
            }
            CREATE PFILE='${BACKUP_PATH}/init${ORACLE_SID}.ora' FROM SPFILE;
EOF
        """, env=self._get_env({
            'BACKUP_PATH': backup_path
        }))

        print ("* Stop database")
        self.stop()

    def clonedb(self, sys_password, master_copy_path):
        """
        Run database from backup using clonedb engine.
        It uses backup files in read-only mode and stores any changes in local data files.
        Backup files should be mounted using nfs share.

        :param sys_password: SYS and SYSTEM users password
        :type sys_password: str
        :param master_copy_path: Path to mounted folder with backup files
        :type master_copy_path: str

        Backup folder structure should be as follows::

        >   ${BACKUP_PATH}/
        >       init${ORACLE_SID}.ora - database parameters file
        >       data/                 - backup files
        """
        assert not self.__db_configured(), 'Database already exists'
        files = glob.glob(master_copy_path + '/init*.ora')
        assert len(files) > 0, 'Backup not found in path ' + master_copy_path
        self.db.sid = re.search('init([^.]+)\.ora', files[0]).group(1)

        print ('\n*** Start clonedb %s' % self.db.sid)

        print ('* Make folders')
        os.makedirs(self.conf_folder + '/dbs')
        os.makedirs(self.conf_folder + '/oradata/' + self.db.sid)
        os.makedirs(self.conf_folder + '/fast_recovery_area/' + self.db.sid)
        os.makedirs(self.conf_folder + '/admin/' + self.db.sid + '/adump')
        os.makedirs(self.conf_folder + '/admin/' + self.db.sid + '/dpdump')
        tempdir = tempfile.mkdtemp()

        print ('* Prepare passwords file')
        self.run("""
            orapwd file=${ORACLE_HOME}/dbs/orapw${ORACLE_SID} password=${SYS_PASSWORD} entries=100
        """, env=self._get_env({
            'SYS_PASSWORD': sys_password
        }))

        print ('* Recover invalid clonedb.pl file')
        replaceAll(self._get_env()['ORACLE_HOME'] + '/rdbms/install/clonedb.pl', '\\o', 'o')

        print ('* Execute configuration')
        self.run("""
            cd ${TEMP_DIR}
            perl ${ORACLE_HOME}/rdbms/install/clonedb.pl ${MASTER_COPY_DIR}/../init${ORACLE_SID}.ora ${TEMP_DIR}/script1.sql ${TEMP_DIR}/script2.sql
            cp ${CLONE_FILE_CREATE_DEST}/init${ORACLE_SID}.ora ${ORACLE_HOME}/dbs/
            sed -i '/^$/d' ${TEMP_DIR}/script1.sql
            sed -i '/^$/d' ${TEMP_DIR}/script2.sql
            echo "*.diagnostic_dest='${DB_CONF}'" >> ${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora
        """, env=self._get_env({
            'TEMP_DIR': tempdir,
            'MASTER_COPY_DIR': master_copy_path + '/data',
            'CLONE_FILE_CREATE_DEST': self.conf_folder + '/oradata/' + self.db.sid,
            'CLONEDB_NAME': self.db.sid
        }))
        self._sqlplus(
            "CREATE SPFILE = '${DB_CONF}/dbs/spfile${ORACLE_SID}.ora' FROM PFILE = '${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora';",
            env=self._get_env())
        pfile = open(self._get_env()['ORACLE_HOME'] + '/dbs/init' + self.db.sid + '.ora', 'w')
        pfile.write("SPFILE='" + self.conf_folder + "/dbs/spfile" + self.db.sid + ".ora'")
        pfile.close()
        replaceAll(os.path.join(tempdir, 'script2.sql'), 'drop tablespace TEMP;', '--drop tablespace TEMP;')
        replaceAll(os.path.join(tempdir, 'script2.sql'), 'create temporary tablespace TEMP;', """
            --create temporary tablespace TEMP;
            create temporary tablespace TEMP2;
            alter database default temporary tablespace TEMP2;
            drop tablespace TEMP;
            create temporary tablespace TEMP;
            alter database default temporary tablespace TEMP;
            drop tablespace TEMP2;
        """)
        self._sqlplus('\n'.join([
            '@%s/script1.sql' % tempdir,
            '@%s/script2.sql' % tempdir
        ]), env=self._get_env({
            'MASTER_COPY_DIR': master_copy_path + '/data',
            'CLONE_FILE_CREATE_DEST': self.conf_folder + '/oradata/' + self.db.sid,
            'CLONEDB_NAME': self.db.sid
        }))

        print ("* Stop database")
        self.stop(force=True)

        print ('* Move some files to configuration folder')
        self._save_files()

    def rundb(self):
        """
        Run pre-configured (using createdb or clonedb functions) database
        """
        assert self.__db_configured(), 'Database not found'

        print ('\n*** Run database %s' % self.db.sid)

        print ('* Link files from configuration folder')
        self._restore_files()

        print ("* Start database")
        self.start()

        print ('* Output log file')
        self.run("""
            tail -n 1000 -F ${DB_CONF}/diag/rdbms/$(echo ${ORACLE_SID} | awk '{print tolower($0)}')/${ORACLE_SID}/alert/log.xml | grep --line-buffered "<txt>" | stdbuf -o0 sed 's/ <txt>//'
        """, verbose_force=True)
