class MyError(Exception):
    """
    Any custom exception to use in our custom classes
    """
    pass


class MyTimeoutError(Exception):
    """
    Custom exception to use in case of command execution timeout
    """
    pass
