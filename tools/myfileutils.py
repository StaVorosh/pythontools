from __future__ import print_function
import fileinput
import os
import shutil

from myerrors import MyError


def movefiles(frm, to, *files):
    """
    Move files from one folder to another (keeping relative paths)

    :param frm: Path to source folder
    :type frm: str
    :param to: Path to target folder
    :type to: str
    :param files: List of relative (to source) file paths to move
    :type files: list
    :raises MyError: If one of files is not present

    For example to move files /from_folder/file1, /from_folder/file2, /from_folder/file3, /from_folder/folder2/file4
    to folder /to_folder, you could run::

    >>  movefiles('/from_folder', '/to_folder', 'file1', 'file2', 'file3', 'folder2/file4')

    or::

    >>  files = ['file1', 'file2', 'file3', 'folder2/file4']
    >>  movefiles('/from_folder', '/to_folder', *files)
    """
    for file in files:
        source = os.path.join(frm, file.strip('/'))
        target = os.path.join(to, file.strip('/'))
        targetdir = os.path.dirname(os.path.abspath(target))
        if os.path.isfile(source):
            if not os.path.isdir(targetdir):
                os.makedirs(targetdir)
            shutil.move(source, target)
            # shutil.copytree(source, target, symlinks=True) https://www.safaribooksonline.com/library/view/python-cookbook-3rd/9781449357337/ch13s07.html
        elif os.path.isdir(source):
            if not os.path.isdir(targetdir):
                os.makedirs(targetdir)
            shutil.move(source, target)
        else:
            raise MyError('Could not move file "%s", it was not found' % source)


def makelinks(frm, to, *files):
    """
    Make symlinks to files (keeping relative paths)

    :param frm: Path to source folder
    :type frm: str
    :param to: Path to target folder
    :type to: str
    :param files: List of relative (to source) file paths to make symlinks in target folder
    :type files: list
    :raises MyError: If one of files is not present

    For example to make symlinks to files /from_folder/file1, /from_folder/file2, /from_folder/file3, /from_folder/folder2/file4
    in folder /to_folder, you could run::

    >>  makelinks('/from_folder', '/to_folder', 'file1', 'file2', 'file3', 'folder2/file4')

    or::

    >>  files = ['file1', 'file2', 'file3', 'folder2/file4']
    >>  makelinks('/from_folder', '/to_folder', *files)
    """
    for file in files:
        source = os.path.join(frm, file.strip('/'))
        target = os.path.join(to, file.strip('/'))
        targetdir = os.path.dirname(os.path.abspath(target))
        if os.path.isfile(source):
            if not os.path.isdir(targetdir):
                os.makedirs(targetdir)
            if os.path.isfile(target):
                os.remove(target)
            os.symlink(source, target)
        elif os.path.isdir(source):
            if not os.path.isdir(targetdir):
                os.makedirs(targetdir)
            if os.path.islink(target):
                os.remove(target)
            if os.path.isdir(target):
                shutil.rmtree(target)
            os.symlink(source, target)
        else:
            raise MyError('Could not make link for file "%s", it was not found' % source)


def replaceAll(file, searchExpr, replaceExpr):
    """
    Find all string occurences of string in file and replace with another string

    :param file: Path to file
    :type file: str
    :param searchExpr: String to look for
    :type searchExpr: str
    :param replaceExpr: String to replace with
    :type replaceExpr: str
    """
    for line in fileinput.input(file, inplace=True):
        print(line.replace(searchExpr, replaceExpr), end='')
