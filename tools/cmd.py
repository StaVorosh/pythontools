from __future__ import print_function
import os
import subprocess
import sys
import signal
import psutil
import time

from myerrors import MyTimeoutError
from multiprocessing import Pipe, Process
from threading import Thread


class Cmd(object):
    verbose = False
    process = None

    def __init__(self, verbose=False):
        """
        Class for executing shell commands

        :param verbose: Set to True to see executed commands and output in stdout. Defaults to False
        :type verbose: bool
        """
        self.verbose = verbose

    def run(self, command, verbose_force=None, env=None, timeout=None):
        """
        Executes shell command

        :param command: Shell command line (or lines)
        :type command: str
        :param verbose_force: Set to True to see output in stdout. Defaults to False
        :type verbose_force: bool
        :param env: Environment variables would be passed to shell. Defaults to None
        :type env: dict
        :param timeout: Timeout in seconds after which execution is stopped. Defaults to None
        :type timeout: float
        :raises MyTimeoutError: If timeout reached while executing

        env parameter example::

        > {
        >   'DATA_HOME': '/data',
        >   'SOME_USER_NAME': 'johny'
        > }
        """

        def _run(verbose, verbose_force, command, env, conn):
            command_arr = [
                '! (. ~/.bash_profile > /dev/null 2>&1)',
                'set -e',
                'set -o pipefail'
            ]
            if verbose:
                command_arr.append('set -x')
            command_arr.append(command)
            process = subprocess.Popen('\n'.join(command_arr), shell=True, stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT, env=env)
            stdout_lines = iter(process.stdout.readline, '')
            for stdout_line in stdout_lines:
                if (verbose and not verbose_force is False) or verbose_force is True:
                    conn.send(stdout_line)
            process.stdout.close()
            conn.close()
            sys.exit(process.wait())

        def _out(conn):
            try:
                while True:
                    while not conn.closed and not conn.poll(1):
                        pass
                    data = conn.recv()
                    if data:
                        print(data, end='')
                    else:
                        break
            except EOFError:
                pass

        parent_conn, child_conn = Pipe()
        self.process = Process(target=_run, args=(self.verbose, verbose_force, command, env, child_conn,))
        self.process.start()

        thread = Thread(target=_out, args=(parent_conn,))
        thread.start()

        self.process.join(timeout)
        if thread.is_alive():
            child_conn.close()
        if self.process.is_alive():
            self.kill()
            raise MyTimeoutError()
        else:
            return_code = self.process.exitcode
            if return_code != 0:
                raise subprocess.CalledProcessError(return_code, command)

    def kill(self):
        if self.process.is_alive():
            try:
                for child in psutil.Process(self.process.pid).children(recursive=True):
                    child.kill()
            except:
                pass
            self.process.join(1)
            if self.process.is_alive():
                try:
                    os.kill(self.process.pid, signal.SIGKILL)
                except:
                    pass
