import ConfigParser
import os


class MyConfig(object):
    __conf_folder = None
    __cfg = None
    __sections = None

    @property
    def conf_folder(self):
        """
        :return: Path to folder where config.ini is stored
        :rtype: str
        """
        return self.__conf_folder

    def __init__(self, conf_folder, sections={}):
        """
        Class whose properties are stored in config file and synchronized in realtime with it

        :param conf_folder: Path to folder where config.ini is stored
        :type conf_folder: str
        :param sections: Available sections and properties to store in config file
        :type sections: dict

        Available sections and properties for config file are set by map::

        >  {
        >    'section1': ['prop1', 'prop2'],
        >    'section2': ['prop3', 'prop4']
        >  }

        Such properties are available as follows::

        >  my_cfg_obj.section1.prop1 = 'value1'
        >  print (my_cfg_obj.section1.prop1) # -> 'value1'
        >  my_cfg_obj.section1.prop2 = 'value2'
        >  my_cfg_obj.section2.prop3 = 'value1'
        >  my_cfg_obj.section2.prop4 = 'value2'

        And is saved to file like this::

        >  [section1]
        >  prop1 = value1
        >  prop2 = value2
        >  [section2]
        >  prop3 = value1
        >  prop4 = value2
        """
        self.__cfg = ConfigParser.ConfigParser()
        self.__conf_folder = conf_folder
        self.__load_config()
        self.__sections = {}
        for sec in sections:
            self.__sections.update({sec: MyConfigSection(self, sec, sections[sec])})

            # def typename(self):
            # return self.__typename

    def __load_config(self):
        if os.path.isfile(self.__conf_folder + '/config.ini'):
            self.__cfg.read(self.__conf_folder + '/config.ini')

    def save_config(self):
        cfgfile = open(self.__conf_folder + '/config.ini', 'w')
        self.__cfg.write(cfgfile)
        cfgfile.close()

    def sections(self):
        return self.__cfg.sections()

    def add_section(self, section):
        self.__cfg.add_section(section)

    def get(self, section, name):
        return self.__cfg.get(section, name)

    def set(self, section, name, value):
        self.__cfg.set(section, name, value)

    def __getattr__(self, name):
        if name.startswith('_' + type(self).__name__ + '_'):
            object.__getattr__(self, name)
        elif name in self.__sections:
            return self.__sections[name]
        else:
            raise AttributeError("'{0}' object has no attribute '{1}'".format(type(self).__name__, name))


class MyConfigSection(object):
    __cfg = None
    __section = ''
    __attributes = []

    def __init__(self, cfg, section, attributes):
        self.__cfg = cfg
        self.__section = section
        self.__attributes = attributes

    def __getattr__(self, name):
        if name.startswith('_' + type(self).__name__ + '_'):
            object.__getattr__(self, name)
        elif name in self.__attributes:
            if not self.__section in self.__cfg.sections():
                return ''
            return self.__cfg.get(self.__section, name)
        else:
            raise AttributeError(
                "'{0}' object has no attribute '{1}'".format(type(self.__cfg).__name__, self.__section + "." + name))

    def __setattr__(self, name, value):
        if name.startswith('_' + type(self).__name__ + '_'):
            object.__setattr__(self, name, value)
        elif name in self.__attributes:
            if not self.__section in self.__cfg.sections():
                self.__cfg.add_section(self.__section)
            self.__cfg.set(self.__section, name, value)
            self.__cfg.save_config()
        else:
            raise AttributeError(
                "'{0}' object has no attribute '{1}'".format(type(self.__cfg).__name__, self.__section + "." + name))