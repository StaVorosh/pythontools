import unittest
import subprocess
import time
from testfixtures import OutputCapture

from tools.myerrors import MyTimeoutError
from tools.cmd import Cmd


class CmdTestCase(unittest.TestCase):
    def test_echo(self):
        sample = 'test@1'
        sample_cmd = 'echo %s' % sample
        with OutputCapture() as output:
            Cmd().run(sample_cmd, verbose_force=True)
        output.compare(sample)

    def test_error_code_12(self):
        sample_code = 12
        with self.assertRaises(subprocess.CalledProcessError) as cm:
            Cmd().run('exit %d' % sample_code)
        self.assertEqual(cm.exception.returncode, sample_code)

    def test_error_code_0(self):
        sample_code = 0
        Cmd().run('exit %d' % sample_code)

    def test_verbose(self):
        sample = 'test@2'
        sample_cmd = 'echo %s' % sample
        with OutputCapture() as output:
            Cmd(verbose=True).run(sample_cmd)
        output.compare('\n'.join([
            '+ %s' % sample_cmd,
            sample
        ]))

    def test_env(self):
        name = 'CMD_TEST_VAR_1'
        value = 'test@1'
        sample_cmd = 'echo ${%s}' % name
        with OutputCapture() as output:
            Cmd().run(sample_cmd, verbose_force=True, env={name: value})
        output.compare(value)

    def test_not_timeout(self):
        sample_timeout = 10
        Cmd().run('sleep %d' % 1, timeout=sample_timeout)

    def test_timeout(self):
        sample_timeout = 1
        start = time.time()
        with self.assertRaises(MyTimeoutError):
            Cmd().run('sleep %d' % (sample_timeout + 10), timeout=sample_timeout)
        end = time.time()
        self.assertLessEqual(end - start, sample_timeout + 1)
