import unittest
from testfixtures import TempDirectory

from tools.myconfig import MyConfig


class MyConfigTestCase(unittest.TestCase):
    def setUp(self):
        self.config_dir = TempDirectory()

    def test_create_config(self):
        conf = MyConfig(self.config_dir.path, {
            'test_section': ['test_property']
        })
        conf.test_section.test_property = 'test_value'
        self.assertEqual(self.config_dir.read('config.ini', 'utf-8').encode('ascii', 'ignore'), '[test_section]\ntest_property = test_value\n\n')

    def test_read_config(self):
        self.config_dir.write('config.ini', u'[test_section]\ntest_property = test_value\n\n', 'utf-8')
        conf = MyConfig(self.config_dir.path, {
            'test_section': ['test_property']
        })
        self.assertEqual(conf.test_section.test_property, 'test_value')

    def test_edit_config(self):
        self.config_dir.write('config.ini', u'[test_section]\ntest_property = test_value\n\n', 'utf-8')
        conf = MyConfig(self.config_dir.path, {
            'test_section': ['test_property', 'test_property2']
        })
        conf.test_section.test_property2 = 'another_test_value'
        self.assertEqual(self.config_dir.read('config.ini', 'utf-8').encode('ascii', 'ignore'),
                         '[test_section]\ntest_property = test_value\ntest_property2 = another_test_value\n\n')

    def tearDown(self):
        TempDirectory.cleanup_all()
