import os
import re
import unittest
from functools import partial

from testfixtures import Replacer, resolve, wrap

from tools.cmd import Cmd


def myreplace(target, replacement, strict=True):
    """
    A decorator to replace a target object for the duration of a test
    function.
    """
    r = Replacer()
    container, method, attribute, t_obj = resolve(target)
    setattr(container, 'orig_' + attribute, getattr(container, attribute))
    return wrap(partial(r.__call__, target, replacement, strict), r.restore)

def myreplaceexecutable(dir_variable, name, script):
    def deco(function):
        def inner(self, *args, **kwargs):
            dir = getattr(self, dir_variable)
            dir.write(name, script, 'utf-8')
            Cmd().run('chmod +x ' + os.path.join(dir.path, name), verbose_force=False)
            return function(self, *args, **kwargs)
        return inner
    return deco

class MyTestCase(unittest.TestCase):
    def myassertRegexpMatches(self, actual, expected, debug=False):
        if debug:
            return self.assertEqual(expected, actual)
        else:
            return self.assertRegexpMatches(actual, re.compile(expected, re.MULTILINE))
