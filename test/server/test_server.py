import os
import unittest
import signal

from multiprocessing import Process

import time

import psutil
from testfixtures import TempDirectory

from tools.cmd import Cmd
from tools.server import Server


class ServerTestCase(unittest.TestCase):
    def setUp(self):
        self.old_sigterm = signal.getsignal(signal.SIGTERM)
        self.config_dir = TempDirectory()
        self.server = Server(False, self.config_dir.path, {
            'test_section': ['test_property1', 'test_property2']
        })

    def test_get_cmd(self):
        self.assertTrue(type(self.server.cmd) is Cmd)

    def test_get_cmd_new_instance(self):
        self.assertNotEqual(self.server.cmd, self.server.cmd)

    def test_save_config(self):
        self.server.test_section.test_property1 = 'test_value'
        self.assertEqual(self.config_dir.read('config.ini', 'utf-8').encode('ascii', 'ignore'),
                         '[test_section]\ntest_property1 = test_value\n\n')

    def test_signal_handling(self):
        signal.signal(signal.SIGTERM, self.old_sigterm)
        sample_time = 10
        child_config_dir = TempDirectory()

        def _run():
            server = Server(False, child_config_dir.path, {})
            cmd = server.cmd
            cmd.run('sleep %d' % (sample_time + 5), verbose_force=True)

        proc = Process(target=_run)
        start = time.time()
        proc.start()
        proc.join(1)
        children = psutil.Process(proc.pid).children(recursive=True)
        os.kill(proc.pid, signal.SIGTERM)
        proc.join(1)
        self.assertFalse(proc.is_alive())
        end = time.time()
        self.assertLessEqual(end - start, 2)

    def tearDown(self):
        signal.signal(signal.SIGTERM, self.old_sigterm)
        TempDirectory.cleanup_all()
