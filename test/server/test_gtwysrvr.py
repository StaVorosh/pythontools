import unittest

import subprocess
from testfixtures import OutputCapture
from testfixtures import TempDirectory

from test.testutils import *
from tools.server import Gtwysrvr


class GtwysrvrTestCase(MyTestCase):
    def setUp(self):
        self.dir = TempDirectory()
        self.dir.makedir('HOME')
        self.dir.makedir('ORACLE_HOME')
        # self.dir.makedir('ORACLE_BASE')
        self.gtwysrvr = Gtwysrvr(True, self.dir.path)

    def get_env(self, args=None):
        env = self.orig__get_env(args)
        env['PATH'] = self.conf_folder + ':' + self.conf_folder + '/ORACLE_HOME/gtwysrvr/bin:' + env['PATH']
        env['HOME'] = self.conf_folder + '/HOME'
        env['ORACLE_HOME'] = self.conf_folder + '/ORACLE_HOME'
        # env['ORACLE_BASE'] = self.conf_folder + '/ORACLE_BASE'
        return env

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/start_ns', 'echo ${TEST_VAR}')
    def test_start(self):
        with OutputCapture() as output:
            self.gtwysrvr.start()
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ start_ns',
                'text_value',
                '$'
            ])
        )
        self.assertEqual(self.gtwysrvr._is_run, True)

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    def test_stop(self):
        self.gtwysrvr._is_run = True
        with OutputCapture() as output:
            self.gtwysrvr.stop()
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ stop_ns',
                'text_value',
                '$'
            ])
        )
        self.assertEqual(self.gtwysrvr._is_run, False)

    def test_stop_not_run(self):
        with OutputCapture() as output:
            self.gtwysrvr.stop()
        self.assertEqual(
            output.captured,
            ''
        )
        self.assertEqual(self.gtwysrvr._is_run, False)

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    def test_stop_force(self):
        self.gtwysrvr._is_run = True
        with OutputCapture() as output:
            self.gtwysrvr.stop(True)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ stop_ns',
                'text_value',
                '$'
            ])
        )
        self.assertEqual(self.gtwysrvr._is_run, False)

    def test_initgtwy_already_conf(self):
        self.gtwysrvr.gateway.host = 'localhost'
        with self.assertRaisesRegexp(Exception, 'Gateway is already configured'):
            self.gtwysrvr.initgtwy('', '', '')

    def prepare_initgtwy_files(self):
        self.dir.write('HOME/templates/gateway_config.rsp', b'sample response file\n', 'utf-8')
        self.dir.write('ORACLE_HOME/gtwysrvr/admin/siebns.dat', b'sample file 1', 'utf-8')
        self.dir.write('ORACLE_HOME/gtwysrvr/sys/test_file', b'sample file 2', 'utf-8')

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'envsubst', 'cat $1')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/start_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/cfgenv.sh', 'export TEST_VAR2=text_value2')
    @myreplaceexecutable('dir', 'ORACLE_HOME/config/config.sh', 'echo ${TEST_VAR2}; echo $@; cat $4')
    def test_initgtwy(self):
        sample_host = 'gtwyhost'
        sample_port = '1267'
        self.prepare_initgtwy_files()
        with OutputCapture() as output:
            self.gtwysrvr.initgtwy(sample_host, sample_port, self.dir.path + '/FS')
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start gateway %s:%s configuration' % (sample_host, sample_port),
                '\* Prepare gateway configuration response file',
                '\+ envsubst .*/templates/gateway_config.rsp',
                '\* Execute configuration',
                '\+ . %s/gtwysrvr/cfgenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR2=text_value2',
                '\+\+ TEST_VAR2=text_value2',
                '\+ cd %s/config' % (self.dir.path + '/ORACLE_HOME'),
                '\+ ./config.sh .*',
                '\+ tee .*/config.log',
                'text_value2',
                '-mode enterprise -responsefile .*/gateway_config.rsp -verbose',
                'sample response file',
                '\+ grep -iq error .*/config.log',
                '\+ exit 0',
                '\* Stop gateway',
                '^\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ stop_ns',
                'text_value',
                '\* Move some files to configuration folder',
                '$'
            ])
        )

        self.assertEqual(self.dir.read('bin/start_ns', 'utf-8').encode('ascii', 'ignore'),
                         'echo ${TEST_VAR}')
        self.assertEqual(self.dir.read('siebenv.sh', 'utf-8').encode('ascii', 'ignore'),
                         'export TEST_VAR=text_value')
        self.assertEqual(self.dir.read('admin/siebns.dat', 'utf-8').encode('ascii', 'ignore'),
                         'sample file 1')
        self.assertEqual(self.dir.read('sys/test_file', 'utf-8').encode('ascii', 'ignore'),
                         'sample file 2')

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'envsubst', 'cat $1')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/start_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/cfgenv.sh', '')
    @myreplaceexecutable('dir', 'ORACLE_HOME/config/config.sh', 'echo some_unexpected_error_text')
    def test_initgtwy_error(self):
        sample_host = 'gtwyhost'
        sample_port = '1267'
        self.prepare_initgtwy_files()
        with OutputCapture() as output:
            with self.assertRaises(subprocess.CalledProcessError) as cm:
                self.gtwysrvr.initgtwy(sample_host, sample_port, self.dir.path + '/FS')
            self.assertEqual(cm.exception.returncode, 1)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start gateway %s:%s configuration' % (sample_host, sample_port),
                '\* Prepare gateway configuration response file',
                '\+ envsubst .*/templates/gateway_config.rsp',
                '\* Execute configuration',
                '\+ . %s/gtwysrvr/cfgenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+ cd %s/config' % (self.dir.path + '/ORACLE_HOME'),
                '\+ ./config.sh .*',
                '\+ tee .*/config.log',
                'some_unexpected_error_text',
                '\+ grep -iq error .*/config.log',
                '\+ exit 1',
                '$'
            ])
        )

    def test_initent_gtwy_not_conf(self):
        with self.assertRaisesRegexp(Exception, 'Gateway isn\'t configured yet. First configure gateway'):
            self.gtwysrvr.initent('', '', '', '', '', '')

    def test_initent_already_conf(self):
        self.gtwysrvr.gateway.host = 'localhost'
        self.gtwysrvr.enterprise.name = 'sample_enterprise'
        with self.assertRaisesRegexp(Exception, 'Enterprise is already configured'):
            self.gtwysrvr.initent('', '', '', '', '', '')

    def prepare_initent_files(self):
        self.dir.write('HOME/templates/tnsnames.ora', b'sample tnsnames file\n', 'utf-8')
        self.dir.write('HOME/templates/enterprise_config.rsp', b'sample response file\n', 'utf-8')
        self.dir.write('admin/siebns.dat', b'sample file 1', 'utf-8')
        self.dir.write('sys/test_file', b'sample file 2', 'utf-8')
        self.dir.write('ORACLE_HOME/gtwysrvr/admin/srvrdefs.run', b'sample file 1', 'utf-8')
        self.dir.write('ORACLE_HOME/gtwysrvr/admin/srvrdefs_sia.run', b'sample file 2', 'utf-8')
        self.dir.write('ORACLE_HOME/gtwysrvr/bin/gateway.cfg', b'sample file 3', 'utf-8')

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'envsubst', 'cat $1')
    @myreplaceexecutable('dir', 'siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'bin/start_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/cfgenv.sh', 'export TEST_VAR2=text_value2')
    @myreplaceexecutable('dir', 'ORACLE_HOME/config/config.sh', 'echo ${TEST_VAR2}; echo $@; cat $4')
    def test_initent(self):
        sample_host = 'gtwyhost'
        sample_port = '1267'
        self.gtwysrvr.gateway.host = sample_host
        self.gtwysrvr.gateway.port = sample_port
        self.gtwysrvr.gateway.siebel_fs = self.dir.path + '/FS'
        sample_ent_name = 'sample_enterprise'
        sample_db_host = 'dbhost'
        sample_db_port = '1287'
        sample_db_sid = 'new_sid'
        sample_db_tns = 'new_tns'
        sample_sadmin_password = 'new_password'
        self.prepare_initent_files()
        with OutputCapture() as output:
            self.gtwysrvr.initent(sample_ent_name, sample_db_host, sample_db_port, sample_db_sid, sample_db_tns,
                                   sample_sadmin_password)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start enterprise %s in gateway %s:%s configuration' % (sample_ent_name, sample_host, sample_port),
                '\* Link files from configuration folder',
                '\* Start gateway',
                '\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ start_ns',
                'text_value',
                '\* Prepare tns names file',
                '\+ envsubst .*/templates/tnsnames.ora',
                '\* Prepare enterprise configuration response file',
                '\+ envsubst .*/templates/enterprise_config.rsp',
                '\* Execute configuration',
                '\+ . %s/gtwysrvr/cfgenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR2=text_value2',
                '\+\+ TEST_VAR2=text_value2',
                '\+ cd %s/config' % (self.dir.path + '/ORACLE_HOME'),
                '\+ ./config.sh .*',
                '\+ tee .*/config.log',
                'text_value2',
                '-mode enterprise -responsefile .*/enterprise_config.rsp -verbose',
                'sample response file',
                '\+ grep -iq error .*/config.log',
                '\+ exit 0',
                '\* Stop gateway',
                '^\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ stop_ns',
                'text_value',
                '\* Move some files to configuration folder',
                '$'
            ])
        )

        self.assertEqual(self.dir.read('admin/srvrdefs.run', 'utf-8').encode('ascii', 'ignore'),
                         'sample file 1')
        self.assertEqual(self.dir.read('admin/srvrdefs_sia.run', 'utf-8').encode('ascii', 'ignore'),
                         'sample file 2')
        self.assertEqual(self.dir.read('bin/gateway.cfg', 'utf-8').encode('ascii', 'ignore'),
                         'sample file 3')

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'envsubst', 'cat $1')
    @myreplaceexecutable('dir', 'siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'bin/start_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/cfgenv.sh', '')
    @myreplaceexecutable('dir', 'ORACLE_HOME/config/config.sh', 'echo some_unexpected_error_text')
    def test_initent_error(self):
        sample_host = 'gtwyhost'
        sample_port = '1267'
        self.gtwysrvr.gateway.host = sample_host
        self.gtwysrvr.gateway.port = sample_port
        self.gtwysrvr.gateway.siebel_fs = self.dir.path + '/FS'
        sample_ent_name = 'sample_enterprise'
        sample_db_host = 'dbhost'
        sample_db_port = '1287'
        sample_db_sid = 'new_sid'
        sample_db_tns = 'new_tns'
        sample_sadmin_password = 'new_password'
        self.prepare_initent_files()
        with OutputCapture() as output:
            with self.assertRaises(subprocess.CalledProcessError) as cm:
                self.gtwysrvr.initent(sample_ent_name, sample_db_host, sample_db_port, sample_db_sid, sample_db_tns,
                                      sample_sadmin_password)
            self.assertEqual(cm.exception.returncode, 1)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start enterprise %s in gateway %s:%s configuration' % (
                sample_ent_name, sample_host, sample_port),
                '\* Link files from configuration folder',
                '\* Start gateway',
                '\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ start_ns',
                'text_value',
                '\* Prepare tns names file',
                '\+ envsubst .*/templates/tnsnames.ora',
                '\* Prepare enterprise configuration response file',
                '\+ envsubst .*/templates/enterprise_config.rsp',
                '\* Execute configuration',
                '\+ . %s/gtwysrvr/cfgenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+ cd %s/config' % (self.dir.path + '/ORACLE_HOME'),
                '\+ ./config.sh .*',
                '\+ tee .*/config.log',
                'some_unexpected_error_text',
                '\+ grep -iq error .*/config.log',
                '\+ exit 1',
                '$'
            ])
        )

    def test_rungtwy_gtwy_not_conf(self):
        with self.assertRaisesRegexp(Exception, 'Gateway isn\'t configured yet. First configure gateway'):
            self.gtwysrvr.rungtwy()

    def test_rungtwy_ent_not_conf(self):
        self.gtwysrvr.gateway.host = 'localhost'
        with self.assertRaisesRegexp(Exception, 'Enterprise isn\'t configured yet. First configure enterprise'):
            self.gtwysrvr.rungtwy()

    def prepare_rungtwy_files(self):
        self.dir.write('HOME/templates/tnsnames.ora', b'sample tnsnames file\n', 'utf-8')
        self.dir.write('admin/siebns.dat', b'sample file 1', 'utf-8')
        self.dir.write('sys/test_file', b'sample file 2', 'utf-8')
        self.dir.write('admin/srvrdefs.run', b'sample file 3', 'utf-8')
        self.dir.write('admin/srvrdefs_sia.run', b'sample file 4', 'utf-8')
        self.dir.write('bin/gateway.cfg', b'sample file 5', 'utf-8')
        self.dir.makedir('ORACLE_HOME/gtwysrvr/log')

    @myreplace('tools.server.Gtwysrvr._get_env', get_env)
    @myreplaceexecutable('dir', 'envsubst', 'cat $1')
    @myreplaceexecutable('dir', 'siebenv.sh', 'export TEST_VAR=text_value')
    @myreplaceexecutable('dir', 'bin/start_ns', 'echo ${TEST_VAR} > ${ORACLE_HOME}/gtwysrvr/log/NameSrvr.log')
    # @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/bin/stop_ns', 'echo ${TEST_VAR}')
    # @myreplaceexecutable('dir', 'ORACLE_HOME/gtwysrvr/cfgenv.sh', '')
    # @myreplaceexecutable('dir', 'ORACLE_HOME/config/config.sh', 'echo some_unexpected_error_text')
    @myreplaceexecutable('dir', 'tail', 'cat $4')
    def test_rungtwy(self):
        sample_host = 'gtwyhost'
        sample_port = '1267'
        self.gtwysrvr.gateway.host = sample_host
        self.gtwysrvr.gateway.port = sample_port
        self.gtwysrvr.gateway.siebel_fs = self.dir.path + '/FS'
        sample_ent_name = 'sample_enterprise'
        self.gtwysrvr.enterprise.name = sample_ent_name
        sample_db_host = 'dbhost'
        sample_db_port = '1287'
        sample_db_sid = 'new_sid'
        sample_db_tns = 'new_tns'
        self.gtwysrvr.db.host = sample_db_host
        self.gtwysrvr.db.port = sample_db_port
        self.gtwysrvr.db.sid = sample_db_sid
        self.gtwysrvr.db.tns = sample_db_tns
        self.prepare_rungtwy_files()
        with OutputCapture() as output:
            self.gtwysrvr.rungtwy()

        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Run gateway %s:%s with enterprise %s' % (sample_host, sample_port, sample_ent_name),
                '\* Prepare tns names file',
                '\+ envsubst .*/templates/tnsnames.ora',
                '\* Link files from configuration folder',
                '\+ touch %s/gtwysrvr/log/NameSrvr.log' % (self.dir.path + '/ORACLE_HOME'),
                '\* Start gateway',
                '\+ . %s/gtwysrvr/siebenv.sh' % (self.dir.path + '/ORACLE_HOME'),
                '\+\+ export TEST_VAR=text_value',
                '\+\+ TEST_VAR=text_value',
                '\+ start_ns',
                '\* Output log file',
                '\+ tail -n 1000 -F %s/gtwysrvr/log/NameSrvr.log' % (self.dir.path + '/ORACLE_HOME'),
                'text_value',
                '$'
            ])
        )

    def tearDown(self):
        TempDirectory.cleanup_all()
