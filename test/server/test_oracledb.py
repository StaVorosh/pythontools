import re
import unittest

import subprocess

import time

from multiprocessing import Process

import signal

import sys
from threading import Thread

from testfixtures import OutputCapture
from testfixtures import TempDirectory

from test.testutils import *
from tools.server import Oracledb


class OracledbTestCase(MyTestCase):
    def setUp(self):
        self.dir = TempDirectory()
        self.dir.makedir('HOME')
        self.dir.makedir('ORACLE_HOME')
        self.dir.makedir('ORACLE_BASE')
        self.oracledb = Oracledb(True, self.dir.path)

    def get_env(self, args=None):
        env = self.orig__get_env(args)
        env['PATH'] = self.conf_folder + ':' + env['PATH']
        env['HOME'] = self.conf_folder + '/HOME'
        env['ORACLE_HOME'] = self.conf_folder + '/ORACLE_HOME'
        env['ORACLE_BASE'] = self.conf_folder + '/ORACLE_BASE'
        return env

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    def test_sqlplus_gen_sql(self):
        sample_string = 'some useful\nstring'
        with OutputCapture() as output:
            self.oracledb._sqlplus(sample_string)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                sample_string,
                'exit',
                '\+ echo 0',
                '0',
                '$'
            ])
        )

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'exit 12')
    def test_sqlplus_exit_code(self):
        with OutputCapture():
            with self.assertRaises(subprocess.CalledProcessError) as cm:
                self.oracledb._sqlplus('')
            self.assertEqual(cm.exception.returncode, 12)

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@')
    def test_start(self):
        with OutputCapture() as output:
            self.oracledb.start()
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'startup',
                'exit',
                '\+ echo 0',
                '0',
                '\+ lsnrctl start',
                'start',
                '$'
            ])
        )
        self.assertEqual(self.oracledb._is_run, True)

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@; exit 5')
    def test_stop(self):
        self.oracledb._is_run = True
        with OutputCapture() as output:
            self.oracledb.stop()
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ lsnrctl stop',
                'stop',
                '\+ true',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'shutdown immediate',
                'exit',
                '\+ echo 0',
                '0',
                '$'
            ])
        )
        self.assertEqual(self.oracledb._is_run, False)

    def test_stop_not_run(self):
        with OutputCapture() as output:
            self.oracledb.stop()
        self.assertEqual(
            output.captured,
            ''
        )
        self.assertEqual(self.oracledb._is_run, False)

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@; exit 5')
    def test_stop_force(self):
        with OutputCapture() as output:
            self.oracledb.stop(True)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^\+ lsnrctl stop',
                'stop',
                '\+ true',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'shutdown immediate',
                'exit',
                '\+ echo 0',
                '0',
                '$'
            ])
        )
        self.assertEqual(self.oracledb._is_run, False)

    def prepare_createdb_files(self, sample_sid, sample_sys_password):
        self.dir.write('ORACLE_HOME/assistants/dbca/templates/General_Purpose.dbc', b'{ORACLE_BASE}#{ORACLE_HOME}/dbs',
                       'utf-8')
        self.dir.write('ORACLE_BASE/cfgtoollogs/dbca/%s.log' % sample_sid, b'sample log\n', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/hc_%s.dat' % sample_sid, b'sample file 1', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/init%s.ora' % sample_sid, b'sample file 2', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/lk%s' % sample_sid, b'sample file 3', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/orapw%s' % sample_sid, b'sample file 4', 'utf-8')

    def test_createdb_already_exists(self):
        self.dir.makedir('oradata')
        with self.assertRaisesRegexp(Exception, 'Database already exists'):
            self.oracledb.createdb('', '')

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@')
    @myreplaceexecutable('dir', 'ORACLE_HOME/bin/dbca', 'echo $3; echo $5; echo $7; echo $9; echo ${13}; echo ${15}')
    def test_createdb(self):
        sample_sid = 'new_sid'
        sample_sys_password = 'new_sys_password'
        self.prepare_createdb_files(sample_sid, sample_sys_password)
        with OutputCapture() as output:
            self.oracledb.createdb(sample_sid, sample_sys_password)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start database %s creation' % sample_sid,
                '\* Prepare database template file',
                '\* Execute creation',
                '\+ %s/bin/dbca .*' % (self.dir.path + '/ORACLE_HOME'),
                sample_sid,
                sample_sid,
                sample_sys_password,
                sample_sys_password,
                '%s/templates/dbca.rsp' % (self.dir.path + '/HOME'),
                '%s/dbtemplate.dbc' % (self.dir.path + '/HOME'),
                "\+ '\[' '!' 1 -eq 1 '\]'",
                '\* Stop database',
                '\+ lsnrctl stop',
                'stop',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'shutdown immediate',
                'exit',
                '\+ echo 0',
                '0',
                '\* Move some files to configuration folder',
                '\+ touch %s/dbs/new_sid_bitmap.dbf' % (self.dir.path + '/ORACLE_HOME'),
                '$'
            ])
        )

        self.assertEqual(self.dir.read('HOME/dbtemplate.dbc', 'utf-8').encode('ascii', 'ignore'),
                         '%s#%s' % (self.dir.path, self.dir.path + '/dbs'));

        self.assertEqual(self.dir.read('dbs/hc_%s.dat' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 1');
        self.assertEqual(self.dir.read('dbs/init%s.ora' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 2');
        self.assertEqual(self.dir.read('dbs/lk%s' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 3');
        self.assertEqual(self.dir.read('dbs/orapw%s' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 4');

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@')
    @myreplaceexecutable('dir', 'ORACLE_HOME/bin/dbca', 'exit 5')
    def test_createdb_error(self):
        sample_sid = 'new_sid'
        sample_sys_password = 'new_sys_password'
        self.prepare_createdb_files(sample_sid, sample_sys_password)
        with OutputCapture() as output:
            with self.assertRaises(subprocess.CalledProcessError) as cm:
                self.oracledb.createdb(sample_sid, sample_sys_password)
            self.assertEqual(cm.exception.returncode, 1)
        self.assertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start database %s creation' % sample_sid,
                '\* Prepare database template file',
                '\* Execute creation',
                '\+ %s/bin/dbca .*' % (self.dir.path + '/ORACLE_HOME'),
                "\+ '\[' '!' 0 -eq 1 '\]'",
                '\+ cat %s/cfgtoollogs/dbca/%s.log' % (self.dir.path + '/ORACLE_BASE', sample_sid),
                'sample log',
                '\+ exit 1',
                '$'
            ])
        )

    def prepare_backupdb_files(self, sample_sid):
        self.dir.write('dbs/hc_%s.dat' % sample_sid, b'sample file 1', 'utf-8')
        self.dir.write('dbs/init%s.ora' % sample_sid, b'sample file 2', 'utf-8')
        self.dir.write('dbs/lk%s' % sample_sid, b'sample file 3', 'utf-8')
        self.dir.write('dbs/orapw%s' % sample_sid, b'sample file 4', 'utf-8')
        self.dir.write('dbs/%s_bitmap.dbf' % sample_sid, b'sample file 5', 'utf-8')

    def test_backupdb_not_found(self):
        with self.assertRaisesRegexp(Exception, 'Database not found'):
            self.oracledb.backupdb('')

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@')
    @myreplaceexecutable('dir', 'ORACLE_HOME/bin/rman', 'cat')
    def test_backupdb(self):
        sample_sid = 'new_sid'
        self.oracledb.db.sid = sample_sid
        self.dir.makedir('oradata')
        self.prepare_backupdb_files(sample_sid)
        backup_dir = TempDirectory()
        with OutputCapture() as output:
            self.oracledb.backupdb(backup_dir.path)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Run database %s backup' % sample_sid,
                '\* Link files from configuration folder',
                '\* Make backup folder',
                '\* Start database',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'startup',
                'exit',
                '\+ echo 0',
                '0',
                '\+ lsnrctl start',
                'start',
                '\* Start backup',
                '\+ %s/bin/rman target=/' % (self.dir.path + '/ORACLE_HOME'),
                '\s*run {',
                '\s*shutdown immediate;',
                '\s*startup mount;',
                '\s*set nocfau;',
                "\s*backup as copy database format '%s/data/%%U';" % backup_dir.path,
                '\s*alter database open;',
                '\s*}',
                "\s*CREATE PFILE='%s/init%s.ora' FROM SPFILE;" % (backup_dir.path, sample_sid),
                '\* Stop database',
                '\+ lsnrctl stop',
                'stop',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'shutdown immediate',
                'exit',
                '\+ echo 0',
                '0',
                '$'
            ])
        )

        self.assertEqual(self.dir.read('ORACLE_HOME/dbs/hc_%s.dat' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 1');
        self.assertEqual(self.dir.read('ORACLE_HOME/dbs/init%s.ora' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 2');
        self.assertEqual(self.dir.read('ORACLE_HOME/dbs/lk%s' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 3');
        self.assertEqual(self.dir.read('ORACLE_HOME/dbs/orapw%s' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 4');
        self.assertEqual(self.dir.read('ORACLE_HOME/dbs/%s_bitmap.dbf' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         'sample file 5');

    def prepare_clonedb_files(self, backup_dir, sample_sid):
        self.dir.makedir('ORACLE_HOME/dbs')
        self.dir.makedir('data')
        backup_dir.write('init%s.ora' % sample_sid, b'test', 'utf-8')
        self.dir.write('ORACLE_HOME/rdbms/install/clonedb.pl', b'sample file \\o with error', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/hc_%s.dat' % sample_sid, b'sample file 1', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/init%s.ora' % sample_sid, b'sample file 2', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/lk%s' % sample_sid, b'sample file 3', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/orapw%s' % sample_sid, b'sample file 4', 'utf-8')
        self.dir.write('ORACLE_HOME/dbs/%s_bitmap.dbf' % sample_sid, b'sample file 5', 'utf-8')

    def test_clonedb_already_exists(self):
        self.dir.makedir('oradata')
        with self.assertRaisesRegexp(Exception, 'Database already exists'):
            self.oracledb.clonedb('', '')

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@')
    @myreplaceexecutable('dir', 'orapwd', 'echo $@')
    @myreplaceexecutable('dir', 'perl', """
        mkdir -p ${DB_CONF}/oradata/${ORACLE_SID};
        touch ${DB_CONF}/oradata/${ORACLE_SID}/init${ORACLE_SID}.ora;
        touch ${TEMP_DIR}/script1.sql; touch ${TEMP_DIR}/script2.sql;
        echo $1; echo $2; echo $3; echo $4
    """)
    @myreplaceexecutable('dir', 'sed', 'exit 0')
    def test_clonedb(self):
        sample_sid = 'new_sid'
        sample_sys_password = 'new_sys_password'
        backup_dir = TempDirectory()
        self.prepare_clonedb_files(backup_dir, sample_sid)
        with OutputCapture() as output:
            self.oracledb.clonedb(sample_sys_password, backup_dir.path)
        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Start clonedb %s' % sample_sid,
                '\* Make folders',
                '\* Prepare passwords file',
                '\+ orapwd .*',
                'file=%s/dbs/orapw%s password=%s entries=100' % (self.dir.path + '/ORACLE_HOME', sample_sid, sample_sys_password),
                '\* Recover invalid clonedb.pl file',
                '\* Execute configuration',
                '\+ cd .*',
                '\+ perl .*',
                '%s/rdbms/install/clonedb.pl' % (self.dir.path + '/ORACLE_HOME'),
                '%s/../init%s.ora' % (backup_dir.path + '/data', sample_sid),
                '.*/script1.sql',
                '.*/script2.sql',
                '\+ cp %s/init%s.ora %s/dbs/' % (self.dir.path + '/oradata/' + sample_sid, sample_sid, self.dir.path + '/ORACLE_HOME'),
                "\+ sed -i '/\^\$/d' .*/script1.sql",
                "\+ sed -i '/\^\$/d' .*/script2.sql",
                "\+ echo '\*\.diagnostic_dest='\\\\''%s'\\\\'''" % self.dir.path,
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                "CREATE SPFILE = '\${DB_CONF}/dbs/spfile\${ORACLE_SID}.ora' FROM PFILE = '\${ORACLE_HOME}/dbs/init\${ORACLE_SID}.ora';",
                'exit',
                '\+ echo 0',
                '0',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                '@.*/script1.sql',
                '@.*/script2.sql',
                'exit',
                '\+ echo 0',
                '0',
                '\* Stop database',
                '\+ lsnrctl stop',
                'stop',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'shutdown immediate',
                'exit',
                '\+ echo 0',
                '0',
                '\* Move some files to configuration folder',
                '$'
            ]),debug=False
        )

        self.assertTrue(os.path.isdir(self.dir.path + '/dbs'))
        self.assertTrue(os.path.isdir(self.dir.path + '/oradata/' + sample_sid))
        self.assertTrue(os.path.isdir(self.dir.path + '/fast_recovery_area/' + sample_sid))
        self.assertTrue(os.path.isdir(self.dir.path + '/admin/' + sample_sid + '/adump'))
        self.assertTrue(os.path.isdir(self.dir.path + '/admin/' + sample_sid + '/dpdump'))

        self.assertEqual(self.dir.read('ORACLE_HOME/rdbms/install/clonedb.pl', 'utf-8').encode('ascii', 'ignore'),
                         'sample file o with error');
        self.assertEqual(self.dir.read('dbs/init%s.ora' % sample_sid, 'utf-8').encode('ascii', 'ignore'),
                         "SPFILE='%s/dbs/spfile%s.ora'" % (self.dir.path, sample_sid));

    def test_rundb_not_found(self):
        with self.assertRaisesRegexp(Exception, 'Database not found'):
            self.oracledb.rundb()

    @myreplace('tools.server.Oracledb._get_env', get_env)
    @myreplaceexecutable('dir', 'sqlplus', 'cat ${4:1}')
    @myreplaceexecutable('dir', 'lsnrctl', 'echo $@')
    @myreplaceexecutable('dir', 'tail', 'cat $4')
    @myreplaceexecutable('dir', 'stdbuf', 'cat')
    def test_rundb(self):
        sample_sid = 'NEW_SID'
        self.dir.makedir('oradata')
        self.oracledb.db.sid = sample_sid
        self.prepare_backupdb_files(sample_sid)
        self.dir.write('diag/rdbms/%s/%s/alert/log.xml' % (sample_sid.lower(), sample_sid), b'<txt>some test</txt>', 'utf-8')
        with OutputCapture() as output:
            self.oracledb.rundb()

        self.myassertRegexpMatches(
            output.captured,
            '\n'.join([
                '^',
                '\*\*\* Run database %s' % sample_sid,
                '\* Link files from configuration folder',
                '\* Start database',
                '\+ sqlplus / as sysdba @.*.sql',
                'WHENEVER SQLERROR EXIT SQL.SQLCODE',
                'WHENEVER OSERROR EXIT FAILURE',
                'startup',
                'exit',
                '\+ echo 0',
                '0',
                '\+ lsnrctl start',
                'start',
                '\* Output log file',
                "\+ grep --line-buffered '<txt>'",
                "\+ stdbuf -o0 sed 's/ <txt>//'",
                '\+\+ echo %s' % sample_sid,
                "\+\+ awk '{print tolower\(\$0\)}'",
                '\+ tail -n 1000 -F %s/diag/rdbms/%s/%s/alert/log.xml' % (self.dir.path, sample_sid.lower(), sample_sid),
                '<txt>some test</txt>',
                '$'
            ])
        )

    def tearDown(self):
        TempDirectory.cleanup_all()

