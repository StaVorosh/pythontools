import os
import unittest

from testfixtures import TempDirectory

from tools.myerrors import MyError
from tools.myfileutils import movefiles, makelinks, replaceAll


class MyFileUtilsTestCase(unittest.TestCase):
  def setUp(self):
    self.from_dir = TempDirectory()
    self.to_dir = TempDirectory()
    num_files = 4
    self.files = []
    for num in range(0, num_files):
      file_name = '%d.txt' % num
      self.from_dir.write(file_name, b'some foo thing', 'utf-8')
      self.files.append(file_name)

  def test_movefiles_without_slashes(self):
    movefiles(self.from_dir.path, self.to_dir.path, *self.files)
    self.from_dir.compare([])
    self.to_dir.compare(self.files)

  def test_movefiles_with_slashes(self):
    files_with_slashes = []
    for file_name in self.files:
      files_with_slashes.append(file_name)
    movefiles(self.from_dir.path, self.to_dir.path, *files_with_slashes)
    self.from_dir.compare([])
    self.to_dir.compare(self.files)

  def test_movefiles_creating_dir(self):
    child_dir = 'child'
    movefiles(self.from_dir.path, os.path.join(self.to_dir.path, child_dir), *self.files)
    self.from_dir.compare([])
    self.to_dir.compare(self.files, path = child_dir)

  def test_movefiles_replacing_file(self):
    self.to_dir.write(self.files[0], b'another foo thing', 'utf-8')
    movefiles(self.from_dir.path, self.to_dir.path, *self.files)
    self.from_dir.compare([])
    self.to_dir.compare(self.files)
    self.assertEqual(self.to_dir.read(self.files[0], 'utf-8').encode('ascii', 'ignore'), 'some foo thing')

  def test_movefiles_error(self):
    with self.assertRaisesRegexp(MyError, 'Could not move file "[^"]*", it was not found'):
      movefiles(self.from_dir.path, self.to_dir.path, 'bad_file_name.txt')

  def test_makelinks_without_slashes(self):
    makelinks(self.from_dir.path, self.to_dir.path, *self.files)
    self.from_dir.compare(self.files)
    self.to_dir.compare(self.files)
    self.from_dir.write(self.files[0], b'another foo thing', 'utf-8')
    self.assertEqual(self.to_dir.read(self.files[0], 'utf-8').encode('ascii', 'ignore'), 'another foo thing')

  def test_makelinks_with_slashes(self):
    files_with_slashes = []
    for file_name in self.files:
      files_with_slashes.append(file_name)
    makelinks(self.from_dir.path, self.to_dir.path, *self.files)
    self.from_dir.compare(self.files)
    self.to_dir.compare(self.files)
    self.from_dir.write(self.files[0], b'another foo thing', 'utf-8')
    self.assertEqual(self.to_dir.read(self.files[0], 'utf-8').encode('ascii', 'ignore'), 'another foo thing')

  def test_replaceAll(self):
    test_string = b'some useful string\nmultiline string\nstring with words\nno more string\n'
    self.from_dir.write(self.files[0], test_string, 'utf-8')
    replaceAll(os.path.join(self.from_dir.path, self.files[0]), 'string', 'text')
    self.assertEqual(self.from_dir.read(self.files[0], 'utf-8').encode('ascii', 'ignore'), test_string.replace('string', 'text'))

  def tearDown(self):
    TempDirectory.cleanup_all()