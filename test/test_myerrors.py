import unittest

from tools.myerrors import MyError, MyTimeoutError


class MyErrorTestCase(unittest.TestCase):
    def test_myError(self):
        with self.assertRaisesRegexp(MyError, 'Test error'):
            raise MyError('Test error')


class MyTimeoutErrorTestCase(unittest.TestCase):
    def test_myTimeoutError(self):
        with self.assertRaisesRegexp(MyTimeoutError, 'Test timeout error'):
            raise MyTimeoutError('Test timeout error')
